<?php require_once '/var/www/config.php';?>
<?php include '/var/www/custom/custom.php';?>
<?php echo $header; ?>

<div class="browse-categories">

	Browse by category »
<?php
	$customObj = new customCode();
	$categories = $customObj->getAllCategories();
	foreach ($categories as $category){
		echo '<a href="'.HTTP_SERVER.'index.php?route=product/category&path='.$category['id'].'">'.$category['name'].'</a>';
	}
?>

</div>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content"><?php echo $content_top; ?>

<h1 style="display: none;"><?php echo $heading_title; ?></h1>

<?php echo $content_bottom; ?></div>

<?php echo $footer; ?>


